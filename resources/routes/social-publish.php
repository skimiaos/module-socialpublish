<?php

$app = Angular::get(OS_APPLICATION_NAME);
$app->addState('config.social_publish','/social','skimia.socialPublish::config','Skimia\SocialPublish\Controllers\SocialPublish@index');
Tiles::makeFromState('social-publish','config.social_publish',[],'Partage Social','os-icon-network-1','pink accent-3');
ConfigPanel::addCategory('website','Site Internet','os-icon-website');

ConfigPanel::registerState('config.social_publish', 'website', 'Comptes Sociaux', 'Gerer vos comptes sur les réseaux sociaux', 'os-icon-network-1');



Route::get('/socialpublish/linked-connect',function(){

    $linkedIn=new Happyr\LinkedIn\LinkedIn('77mqje35bjvuqk', 'VFVcJ77Lqn8141mh');

    if ($linkedIn->isAuthenticated()) {
        //we know that the user is authenticated now. Start query the API
        $user=$linkedIn->get('v1/people/~:(firstName,lastName)');
        return "Welcome ".$user['firstName'];

        exit();
    } elseif ($linkedIn->hasError()) {
        return "User canceled the login.";
        exit();
    }

//if not authenticated
    $url = $linkedIn->getLoginUrl();
    return "<a href='$url'>Login with LinkedIn</a>";

});


Route::get('/socialpublish/facebook-connect-redirect',['uses'=>'Skimia\SocialPublish\Controllers\SocialPublish@facebookConnectRedirect']);
Route::get('/socialpublish/facebook-connect',['uses'=>'Skimia\SocialPublish\Controllers\SocialPublish@facebookConnect']);



Route::get('/socialpublish/twitter-connect-redirect',['uses'=>'Skimia\SocialPublish\Controllers\SocialPublish@twitterConnectRedirect']);
Route::get('/socialpublish/twitter-connect',['uses'=>'Skimia\SocialPublish\Controllers\SocialPublish@twitterConnect']);


Route::get('/socialpublish/linkedin-connect',['uses'=>'Skimia\SocialPublish\Controllers\SocialPublish@linkedinConnect']);

Route::post('/socialpublish/update',['uses'=>'Skimia\SocialPublish\Controllers\SocialPublish@update']);