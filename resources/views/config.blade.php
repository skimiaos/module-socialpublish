@extends('skimia.backend::layouts.page')


@block('page.content')
<style>
    .bar{
        padding: 10px !important;
        border-bottom: 1px solid darkgray;
    }
    .check{


    }
    .check:before{
        font-size: 50px;
        vertical-align: middle;
    }
    .spacer{
        width: 20px;
        display: inline-block;
    }
</style>
<os-container os-flex="1" direction="column">
    <div>

        <div class="col m12 bar">
            <a class="btn" ng-class="{'red darken-3':linkedin}" href="{{url('/socialpublish/linkedin-connect')}}" target="_blank">

                Connecter <i class="os-icon-linkedin"></i>
            </a>
            <span ng-show="linkedin" class="spacer"></span>
            <a class="btn red darken-3" ng-click="update('linkedin','activate',false)" ng-show="linkedin && linkedin_active">Publication automatique</a>
            <a class="btn-flat" ng-click="update('linkedin','activate', true)" ng-show="linkedin && !linkedin_active">Désactivé</a>
            <span ng-show="linkedin" class="spacer"></span>
            <span ng-show="linkedin">Poster en tant que :</span>
            <a class="btn-flat" ng-click="update('linkedin','mode', 'user')" ng-show="linkedin && linkedin_mode == 'page'">Entreprise</a>
            <a class="btn-flat" ng-click="update('linkedin','mode', 'page')" ng-show="linkedin && linkedin_mode == 'user'">Utilisateur</a>
            <os-select ng-show="linkedin && linkedin_mode == 'page'" name="linked_entreprises"
                       ng-model="linkedin_page_id"

                       options="linkedin_pages"

                       placeholder="Choisissez une option">
                Label
            </os-select>
            <span ng-show="linkedin" class="spacer"></span>
            <span ng-show="linkedin">Expiration du jeton : <b>@{{ linkedin_expire.replace('days','jours').replace('day','jour').replace('month','mois') }}</b></span>
            <i class="os-icon-check-2 check green-text" ng-show="linkedin"></i>
            <i class="os-icon-cancel-4 check red-text text-darken-2" ng-hide="linkedin"></i>
        </div>
        <div class="col m12 bar">
            <a class="btn" ng-class="{'red darken-3':facebook}" target="_blank" href="{{url('/socialpublish/facebook-connect-redirect')}}">Connecter <i class="os-icon-facebook"></i></a>
            <span ng-show="facebook" class="spacer"></span>
            <a class="btn red darken-3" ng-click="update('facebook','activate', false)" ng-show="facebook && facebook_active">Publication automatique</a>
            <a class="btn-flat" ng-click="update('facebook','activate', true)" ng-show="facebook && !facebook_active">Désactivé</a>
            <span ng-show="facebook" class="spacer"></span>
            <span ng-show="facebook">Poster en tant que :</span>
            <a class="btn-flat" ng-click="update('facebook','mode', 'user')" ng-show="facebook && facebook_mode == 'page'">Entreprise</a>
            <a class="btn-flat" ng-click="update('facebook','mode', 'page')" ng-show="facebook && facebook_mode == 'user'">Utilisateur</a>
            <os-select ng-show="facebook && facebook_mode == 'page'" name="facebook_entreprises"
                       ng-model="facebook_page_id"

                       options="facebook_pages"

                       placeholder="Choisissez une option">
                Label
            </os-select>
            <span ng-show="linkedin" class="spacer"></span>
            <span ng-show="facebook">Expiration du jeton : <b>@{{ facebook_expire.replace('days','jours').replace('day','jour').replace('month','mois') }}</b></span>
            <i class="os-icon-check-2 check green-text" ng-show="facebook"></i>
            <i class="os-icon-cancel-4 check red-text text-darken-2" ng-hide="facebook"></i>
        </div>
        <div class="col m12 bar">
            <a class="btn" ng-class="{'red darken-3':twitter}" target="_blank" href="{{url('/socialpublish/twitter-connect-redirect')}}">Connecter <i class="os-icon-twitter"></i></a>

            <span ng-show="twitter" class="spacer"></span>
            <a class="btn red darken-3" ng-click="update('twitter','activate', false)" ng-show="twitter && twitter_active">Publication automatique</a>
            <a class="btn-flat" ng-click="update('twitter','activate', true)" ng-show="twitter && !twitter_active">Désactivé</a>
            <span ng-show="linkedin" class="spacer"></span>
            <i class="os-icon-check-2 check green-text" ng-show="twitter"></i>
            <i class="os-icon-cancel-4 check red-text text-darken-2" ng-hide="twitter"></i>
        </div>
    </div>




</os-container>



@endoverride

{{-- Angular controller --}}
@Controller
@AddDependency('$sce')
//<script>
    $scope.$watch('linkedin_page_id',function(news,old){
        if(angular.isDefined(news))
            $scope.update('linkedin','page_id', $scope.linkedin_page_id);
    });
    $scope.$watch('facebook_page_id',function(news,old){
        if(angular.isDefined(news))
            $scope.update('facebook','page_id', $scope.facebook_page_id);
    });
    $scope.update = function($type,$action,$params){
        $url = '{{ url('/socialpublish/update')  }}';

        $angular_response($scope).post($url,{
            type:$type,
            action:$action,
            params:$params
        }).success(function (data) {
            switch($action){
                case 'activate':

                        switch ($type){
                            case 'facebook':
                                $scope.facebook_active = $params;
                                break;
                            case 'twitter':
                                $scope.twitter_active = $params;
                                break;
                            case 'linkedin':
                                $scope.linkedin_active = $params;
                                break;
                        }

                    break;
                case 'mode':
                    switch ($type){
                        case 'facebook':
                            $scope.facebook_mode = $params;
                            break;
                        case 'linkedin':
                            $scope.linkedin_mode = $params;
                            break;
                    }
                    break;
            }
        });
    }
    //</script>
@EndController