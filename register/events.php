<?php

use Carbon\Carbon;

function check_facebook(){
    if(empty(\Config::get('skimia.socialPublish::tokens.facebook.userToken')))
        return false;
    if(!Config::get('skimia.socialPublish::tokens.facebook.active'))
        return false;

    $expire = \Config::get('skimia.socialPublish::tokens.facebook.expirationDate');

    if($expire != 'infinite'){
        $cnow = Carbon::now('Europe/Paris');
        $cn = Carbon::createFromTimestamp($expire);

        if($cnow > $cn ){
            return false;
        }
    }


    if(Config::get('skimia.socialPublish::tokens.facebook.mode') == 'page'){

        if(empty(\Config::get('skimia.socialPublish::tokens.facebook.pageToken')))
            return false;

        /*$expire = \Config::get('skimia.socialPublish::tokens.facebook.pageTokenExpirationDate');

        $cnow = Carbon::now('Europe/Paris');
        $cn = Carbon::createFromTimestamp($expire);

        if($cnow > $cn ){
            return false;
        }*/

    }




    return true;
}

function check_twitter(){
    if(empty(\Config::get('skimia.socialPublish::tokens.twitter.userToken')))
        return false;
    if(!Config::get('skimia.socialPublish::tokens.twitter.active'))
        return false;

    return true;
}

function check_linkedin(){
    if(empty(\Config::get('skimia.socialPublish::tokens.linkedin.userToken')))
        return false;
    if(!Config::get('skimia.socialPublish::tokens.linkedin.active'))
        return false;

    $expire = \Config::get('skimia.socialPublish::tokens.linkedin.expirationDate');

    $cnow = Carbon::now('Europe/Paris');
    $cn = Carbon::createFromTimestamp($expire);

    if($cnow > $cn ){
        return false;
    }

    return true;
}

\Skimia\News\Data\Forms\PostsCRUDForm::registerCRUDFormEvent('afterCreateSave',function(\Skimia\News\Data\Models\Post $post){


    if(check_twitter()){
        require_once (module_lib('skimia.socialPublish','codebird','codebird.php'));
        \Codebird\Codebird::setConsumerKey(\Config::get('skimia.socialPublish::tokens.twitter.consumerKey'),
            \Config::get('skimia.socialPublish::tokens.twitter.consumerSecret'));

        $cb = \Codebird\Codebird::getInstance();
        session_start();

        $cb->setToken(\Config::get('skimia.socialPublish::tokens.twitter.userToken'),\Config::get('skimia.socialPublish::tokens.twitter.userTokenSecret'));

        /**
         * UPLOAD
         */
        try {
            $upload = $cb->media_upload(array(
                'media' => $post->picture
            ));

            $reply = (array) $cb->statuses_update([
                'status' => $post->name.' '.url('/post/'.$post->slug),
                'media_ids' => $upload->media_id_string,
            ]);
            AResponse::addMessage('Article publié sur Twitter');
        } catch(\Exception $e) {
            echo 'Twitter returned an error: ' . $e->getMessage();
            exit;
        }
    }


    if(check_facebook()){
        $fb = new \Facebook\Facebook([
            'app_id' => \Config::get('skimia.socialPublish::tokens.facebook.appId'),
            'app_secret' => \Config::get('skimia.socialPublish::tokens.facebook.appSecret'),
            'default_graph_version' => 'v2.2',
        ]);

        $linkData = [
            'link' => url().'/post/'.$post->getSlug(),
            'message' => $post->title,
        ];

        try {
            // Returns a `Facebook\FacebookResponse` object
            if(Config::get('skimia.socialPublish::tokens.facebook.mode') == 'page')
                $response = $fb->post('/me/feed', $linkData, \Config::get('skimia.socialPublish::tokens.facebook.pageToken'));
            else
                $response = $fb->post('/me/feed', $linkData, \Config::get('skimia.socialPublish::tokens.facebook.userToken'));
            AResponse::addMessage('Article publié sur Facebook');
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }





    if(check_linkedin()){
        try{
            $postedData = [
                'comment'=>'Retrouvez notre nouvel article sur '.url().'/post/'.$post->getSlug(),
                'content'=>[
                    'title'=> $post->title,
                    'description'=>strip_tags($post->content_short),
                    'submitted-url'=>url().'/post/'.$post->getSlug(),
                    'submitted-image-url'=>$post->picture
                ],
                'visibility'=>[
                    'code'=>'anyone'
                ]
            ];
            $postedData = json_encode($postedData);
            $token = \Config::get('skimia.socialPublish::tokens.linkedin.userToken');
            if(\Config::get('skimia.socialPublish::tokens.linkedin.mode') == 'user')
                $resource = '/v1/people/~/shares';
            else
                $resource = '/v1/companies/'.Config::get('skimia.socialPublish::tokens.linkedin.pageId').'/shares';
            $params = array('oauth2_access_token' => $token, 'format' => 'json');
            $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);


            $context_options = array (
                'http' => array (
                    'method' => 'POST',
                    'header'=> "Content-type: application/json\r\n"
                        . "Content-Length: " . strlen($postedData) . "\r\n",
                    'content' => $postedData
                )
            );
            $context = stream_context_create($context_options);
            $response = file_get_contents($url, false, $context);
            AResponse::addMessage('Article publié sur LinkedIn');
        }catch(\Exception $e){

        }
    }




});