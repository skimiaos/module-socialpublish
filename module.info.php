<?php

return [
    'name'        => 'Skimia SocialPublish',
    'author'      => 'Skimia Agency',
    'description' => 'Gestion api',
    'namespace'   => 'Skimia\\SocialPublish',
    'require'     => ['skimia.modules','skimia.blade','skimia.form','skimia.angular','skimia.auth','skimia.backend','skimia.pages']
];