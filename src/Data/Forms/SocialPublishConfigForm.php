<?php
namespace Skimia\SocialPublish\Data\Forms;


use Skimia\Config\Form\ConfigForm;


class SocialPublishConfigForm extends ConfigForm{

    protected $id = 'website.social_publish';
    protected $name = 'Comptes Sociaux';
    protected $description = 'Gerer vos comptes sur les réseaux sociaux';
    protected $icon = 'os-icon-network-1';
    protected $saveSuccessMessage = "Mise a jour terminée les prochains retours utiliseront les nouvelles configurations.";

    protected $template = 'skimia.socialPublish::config';

    protected $configs = [
        'skimia.contacts::emails.retours'=>['label'=>'Email de retour(sépares par une virgule)','type'=>'text',
            '__alias'=>'email'],
    ];
}