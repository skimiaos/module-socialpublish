<?php


namespace Skimia\SocialPublish\Controllers;


use Facebook\Helpers\FacebookRedirectLoginHelper;
use Request;
use Response;
use Carbon\Carbon;
use Redirect;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\File;
use League\OAuth2\Client\Provider\LinkedIn;
class SocialPublish extends \Controller{


    public function index(){

        $data = [
            'facebook'=>false,
            'linkedin'=>false,
            'twitter'=>false
        ];

        if(!empty(\Config::get('skimia.socialPublish::tokens.facebook.userToken'))){
            $data['facebook'] = true;
            $data['facebook_active'] = \Config::get('skimia.socialPublish::tokens.facebook.active');

            $expire = \Config::get('skimia.socialPublish::tokens.facebook.expirationDate');

            if($expire == 'infinite'){
                $infinite = true;
                $data['facebook_expire'] = 'infinite';
            }

            else{
                $infinite = false;
                $cnow = Carbon::now('Europe/Paris');
                $cn = Carbon::createFromTimestamp($expire);
                $data['facebook_expire'] = $cn->diffForHumans($cnow,true);
            }

            if(!$infinite && $cnow > $cn ){

                $data['facebook'] = false;
            }else{
                $fb = $this->getFB();
                /**
                 * @var $response \Facebook\FacebookResponse
                 */
                $response = $fb->get('/me/accounts',\Config::get('skimia.socialPublish::tokens.facebook.userToken'));


                $fbData = $response->getDecodedBody()['data'];

                $data['facebook_pages'] = [];
                foreach ($fbData as $page) {
                    if(isset( $page['access_token']) && in_array('CREATE_CONTENT',$page['perms']))
                        $data['facebook_pages'][$page['id']] = $page['name'];
                }

                $data['facebook_mode'] = \Config::get('skimia.socialPublish::tokens.facebook.mode');

                $data['facebook_page_id'] = \Config::get('skimia.socialPublish::tokens.facebook.pageId');


            }

        }

        if(!empty(\Config::get('skimia.socialPublish::tokens.twitter.userToken'))){
            $data['twitter'] = true;
            $data['twitter_active'] = \Config::get('skimia.socialPublish::tokens.twitter.active');
        }

        if(!empty(\Config::get('skimia.socialPublish::tokens.linkedin.userToken'))){
            $data['linkedin'] = true;
            $data['linkedin_active'] = \Config::get('skimia.socialPublish::tokens.linkedin.active');
            $expire = \Config::get('skimia.socialPublish::tokens.linkedin.expirationDate');

            $cnow = Carbon::now('Europe/Paris');
            $cn = Carbon::createFromTimestamp($expire);
            $data['linkedin_expire'] = $cn->diffForHumans($cnow,true);

            if($cnow > $cn ){
                $data['linkedin'] = false;
            }
            else{
                $token = \Config::get('skimia.socialPublish::tokens.linkedin.userToken');
                $resource = '/v1/companies';
                $params = array('oauth2_access_token' => $token, 'format' => 'json','is-company-admin'=>'true');
                $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);


                $context_options = array (
                    'http' => array (
                        'method' => 'GET',
                        'header'=> "Content-type: application/json\r\n"
                    )
                );
                $context = stream_context_create($context_options);
                $response = file_get_contents($url, false, $context);
                $dataLinked = json_decode($response,true);

                $data['linkedin_pages'] = [];
                foreach ($dataLinked['values'] as $page) {
                    $data['linkedin_pages'][$page['id']] = $page['name'];
                }

                $data['linkedin_mode'] = \Config::get('skimia.socialPublish::tokens.linkedin.mode');

                $data['linkedin_page_id'] = \Config::get('skimia.socialPublish::tokens.linkedin.pageId');
            }
        }





        return \AResponse::r($data);
    }

    public function facebookConnectRedirect(){

        session_start();
        $fb = new \Facebook\Facebook([
            'app_id' => \Config::get('skimia.socialPublish::tokens.facebook.appId'),
            'app_secret' => \Config::get('skimia.socialPublish::tokens.facebook.appSecret'),
            'default_graph_version' => 'v2.2',
        ]);
        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email','publish_actions','manage_pages','publish_pages']; // Optional permissions
        $loginUrl = $helper->getLoginUrl(url('/socialpublish/facebook-connect'), $permissions);

        return Redirect::to($loginUrl);

    }

    protected function getFB(){
        session_start();
        return new \Facebook\Facebook([
            'app_id' =>\Config::get('skimia.socialPublish::tokens.facebook.appId'),
            'app_secret' => \Config::get('skimia.socialPublish::tokens.facebook.appSecret'),
            'default_graph_version' => 'v2.2',
        ]);
    }
    public function facebookConnect(){
        global $app;
        $fb = $this->getFB();

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

// Logged in
        /*echo '<h3>Access Token</h3>';
        var_dump($accessToken->getValue());*/

// The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);

// Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId(\Config::get('skimia.socialPublish::tokens.facebook.appId'));
// If you know the user ID this access token belongs to, you can validate it here
//$tokenMetadata->validateUserId('123');
        $infinite = false;
        if($tokenMetadata->getExpiresAt() > new \DateTime(date("c", 0)))
            $tokenMetadata->validateExpiration();
        else
            $infinite = true;
        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }

            /*echo '<h3>Long-lived</h3>';
            var_dump($accessToken->getValue());*/
        }

        //$_SESSION['fb_access_token'] = (string) $accessToken;

        $app['configwriter']->write('skimia.socialPublish::tokens.facebook.userToken',(string) $accessToken);
        if(!$infinite)
            $app['configwriter']->write('skimia.socialPublish::tokens.facebook.expirationDate',(string) $accessToken->getExpiresAt()->getTimestamp());
        else
            $app['configwriter']->write('skimia.socialPublish::tokens.facebook.expirationDate','infinite');

        die('<script>window.close();</script>');


    }

    public function update(){

        global $app;

        $action = \Input::get('action');
        $type = \Input::get('type');
        $params = \Input::get('params');

        switch($action){
            case 'activate':

                switch ($type){
                    case 'facebook':
                        $app['configwriter']->write('skimia.socialPublish::tokens.facebook.active', (boolval($params)));
                        break;
                    case 'twitter':
                        $app['configwriter']->write('skimia.socialPublish::tokens.twitter.active', (boolval($params)));
                        break;
                    case 'linkedin':
                        $app['configwriter']->write('skimia.socialPublish::tokens.linkedin.active', (boolval($params)));
                        break;
                }

                break;

            case 'mode':

                switch ($type){
                    case 'facebook':
                        $app['configwriter']->write('skimia.socialPublish::tokens.facebook.mode', $params);
                        break;
                    case 'linkedin':
                        $app['configwriter']->write('skimia.socialPublish::tokens.linkedin.mode', $params);
                        break;
                }

                break;

            case 'page_id':

                switch ($type){
                    case 'facebook':
                        $app['configwriter']->write('skimia.socialPublish::tokens.facebook.pageId', $params);
                        $fb = $this->getFB();
                        /**
                         * @var $response \Facebook\FacebookResponse
                         */
                        $response = $fb->get('/me/accounts',\Config::get('skimia.socialPublish::tokens.facebook.userToken'));


                        $fbData = $response->getDecodedBody()['data'];

                        $data['facebook_pages'] = [];
                        foreach ($fbData as $page) {
                            if(isset( $page['access_token']) && in_array('CREATE_CONTENT',$page['perms']))
                                if($page['id'] == $params){
                                    $app['configwriter']->write('skimia.socialPublish::tokens.facebook.pageToken', $page['access_token']);
                                }
                        }
                        break;
                    case 'linkedin':
                        $app['configwriter']->write('skimia.socialPublish::tokens.linkedin.pageId', $params);
                        break;
                }

                break;
        }

        \AResponse::addMessage('Sauvegardé');
        return \AResponse::r([]);
    }

    public function twitterConnectRedirect(){
        require_once (module_lib('skimia.socialPublish','codebird','codebird.php'));
        \Codebird\Codebird::setConsumerKey(\Config::get('skimia.socialPublish::tokens.twitter.consumerKey'),
            \Config::get('skimia.socialPublish::tokens.twitter.consumerSecret'));

        $cb = \Codebird\Codebird::getInstance();
        session_start();

        $reply = $cb->oauth_requestToken([
            'oauth_callback' => url('/socialpublish/twitter-connect')
        ]);
        // store the token
        $cb->setToken($reply->oauth_token, $reply->oauth_token_secret);
        $_SESSION['oauth_token'] = $reply->oauth_token;
        $_SESSION['oauth_token_secret'] = $reply->oauth_token_secret;
        $_SESSION['oauth_verify'] = true;

        // redirect to auth website
        $auth_url = $cb->oauth_authorize();
        header('Location: ' . $auth_url);
        die();
    }

    public function twitterConnect(){
        global $app;
        require_once (module_lib('skimia.socialPublish','codebird','codebird.php'));
        \Codebird\Codebird::setConsumerKey(\Config::get('skimia.socialPublish::tokens.twitter.consumerKey'),
            \Config::get('skimia.socialPublish::tokens.twitter.consumerSecret'));

        $cb = \Codebird\Codebird::getInstance();
        session_start();

        if (isset($_GET['oauth_verifier']) && isset($_SESSION['oauth_verify'])) {
            // verify the token
            $cb->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            unset($_SESSION['oauth_verify']);

            // get the access token
            $reply = $cb->oauth_accessToken([
                'oauth_verifier' => $_GET['oauth_verifier']
            ]);

            // store the token (which is different from the request token!)

            $app['configwriter']->write('skimia.socialPublish::tokens.twitter.userToken', (string) $reply->oauth_token);
            $app['configwriter']->write('skimia.socialPublish::tokens.twitter.userTokenSecret', (string) $reply->oauth_token_secret);
            // send to same URL, without oauth GET parameters
        }
        die('<script>window.close();</script>');
    }

    public function linkedinConnect(){
        global $app;
        $provider = new LinkedIn(\Config::get('skimia.socialPublish::tokens.linkedin'));

        if ( !Input::has('code')) {
            // If we don't have an authorization code, get one
            $provider->authorize();
        } else {
            try {
                // Try to get an access token (using the authorization code grant)
                $t = $provider->getAccessToken('authorization_code', array('code' => Input::get('code'),'scope'=>'rw_company_admin'));

                $app['configwriter']->write('skimia.socialPublish::tokens.linkedin.userToken', (string) $t->getToken());
                $app['configwriter']->write('skimia.socialPublish::tokens.linkedin.expirationDate', (string) $t->getExpires());

                die('<script>window.close();</script>');
                try {
                    // We got an access token, let's now get the user's details
                    //$userDetails = $provider->getUserDetails($t);
                    $resource = '/v1/people/~:(firstName,lastName,pictureUrl,positions,educations,threeCurrentPositions,threePastPositions,dateOfBirth,location)';
                    $params = array('oauth2_access_token' => $t->getToken(), 'format' => 'json');
                    $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);
                    $context = stream_context_create(array('http' => array('method' => 'GET')));
                    $response = file_get_contents($url, false, $context);
                    $data = json_decode($response);
                    dd($data);
                } catch (Exception $e) {
                    return 'Unable to get user details';
                }

            } catch (Exception $e) {
                return 'Unable to get access token';
            }
        }
    }
}